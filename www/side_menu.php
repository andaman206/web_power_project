<?php 
$_part = $_SESSION["active"]; 
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="Home_ds.php" class="brand-link">
      <img src="Lte_v3/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">DEMO</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="<?php if ($_part=="/admin_system_school/Home_ds.php") {echo "nav-link active"; } else  {echo "nav-link";}?> <?php if ($_part=="/admin_system_school/Student_ds.php") {echo "nav-link active"; } else  {echo "nav-link";}?> <?php if ($_part=="/admin_system_school/Teacher_ds.php") {echo "nav-link active"; } else  {echo "nav-link";}?> <?php if ($_part=="/admin_system_school/Staff_ds.php") {echo "nav-link active"; } else  {echo "nav-link";}?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="./Home_ds.php" class="<?php if ($_part=="/admin_system_school/Home_ds.php") {echo "nav-link active"; } else  {echo "nav-link";}?>">
                <i class="fas fa-home"></i>
                  <p>หน้าแรก</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="<?php if ($_part=="/admin_system_school/Teacher_ds.php") {echo "nav-link active"; } else  {echo "nav-link";}?>">
                <i class="fas fa-school"></i>
                  <p>ภาคเหนือ</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="<?php if ($_part=="/admin_system_school/Teacher_ds.php") {echo "nav-link active"; } else  {echo "nav-link";}?>">
                <i class="fas fa-school"></i>
                  <p>ภาคตะวันออกเฉียงเหนือ</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="<?php if ($_part=="/admin_system_school/Teacher_ds.php") {echo "nav-link active"; } else  {echo "nav-link";}?>">
                <i class="fas fa-school"></i>
                  <p>ภาคกลาง</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="<?php if ($_part=="/admin_system_school/Teacher_ds.php") {echo "nav-link active"; } else  {echo "nav-link";}?>">
                <i class="fas fa-school"></i>
                  <p>ภาคใต้</p>
                </a>
              </li>
            </ul>
          </li>
          <!-- /dashboard -->

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>