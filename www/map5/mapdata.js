var simplemaps_countrymap_mapdata={
  main_settings: {
   //General settings
    width: "300", //'700' or 'responsive'
    background_color: "#FFFFFF",
    background_transparent: "yes",
    border_color: "#ffffff",
    
    //State defaults
    state_description: "State description",
    state_color: "#88A4BC",
    state_hover_color: "#3B729F",
    state_url: "",
    border_size: 1.5,
    all_states_inactive: "no",
    all_states_zoomable: "yes",
    
    //Location defaults
    location_description: "Location description",
    location_url: "",
    location_color: "#FF0067",
    location_opacity: 0.8,
    location_hover_opacity: 1,
    location_size: "30",
    location_type: "square",
    location_image_source: "frog.png",
    location_border_color: "#FFFFFF",
    location_border: 2,
    location_hover_border: 2.5,
    all_locations_inactive: "no",
    all_locations_hidden: "no",
    
    //Label defaults
    label_color: "#d5ddec",
    label_hover_color: "#d5ddec",
    label_size: "0",
    label_font: "Arial",
    hide_labels: "no",
    hide_eastern_labels: "no",
   
    //Zoom settings
    zoom: "yes",
    manual_zoom: "yes",
    back_image: "no",
    initial_back: "no",
    initial_zoom: "-1",
    initial_zoom_solo: "no",
    region_opacity: 1,
    region_hover_opacity: 0.6,
    zoom_out_incrementally: "yes",
    zoom_percentage: 0.99,
    zoom_time: 0.5,
    
    //Popup settings
    popup_color: "white",
    popup_opacity: 0.9,
    popup_shadow: 1,
    popup_corners: 5,
    popup_font: "12px/1.5 Verdana, Arial, Helvetica, sans-serif",
    popup_nocss: "no",
    
    //Advanced settings
    div: "map",
    auto_load: "yes",
    url_new_tab: "yes",
    images_directory: "default",
    fade_time: 0.1,
    link_text: "View Website",
    popups: "no",
    state_image_url: "",
    state_image_position: "",
    location_image_url: ""
  },
  state_specific: {
    THA374: {
      name: "Mae Hong Son",
      description: " "
    },
    THA375: {
      name: "Chumphon",
      description: "กำลังการผลิต 68,000 ลูกบาศก์เมตรต่อวัน"
    },
    THA376: {
      name: "Nakhon Si Thammarat",
      description: " "
    },
    THA377: {
      name: "Phuket",
      description: " "
    },
    THA378: {
      name: "Phangnga",
      description: " "
    },
    THA379: {
      name: "Ranong",
      description: " "
    },
    THA380: {
      name: "Surat Thani",
      description: "กำลังการผลิต 83,750 ลูกบาศก์เมตรต่อวัน"
    },
    THA382: {
      name: "Krabi",
      description: " "
    },
    THA383: {
      name: "Phatthalung",
      description: " "
    },
    THA385: {
      name: "Satun",
      description: " "
    },
    THA386: {
      name: "Songkhla",
      description: " "
    },
    THA387: {
      name: "Trang",
      description: " "
    },
    THA388: {
      name: "Yala",
      description: " "
    },
    THA389: {
      name: "Chiang Rai",
      description: " "
    },
    THA390: {
      name: "Chiang Mai",
      description: " "
    },
    THA391: {
      name: "Lampang",
      description: " "
    },
    THA392: {
      name: "Lamphun",
      description: " "
    },
    THA393: {
      name: "Nan",
      description: " "
    },
    THA394: {
      name: "Phayao",
      description: " "
    },
    THA395: {
      name: "Phrae",
      description: " "
    },
    THA396: {
      name: "Phitsanulok",
      description: " "
    },
    THA397: {
      name: "Sukhothai",
      description: " "
    },
    THA398: {
      name: "Uttaradit",
      description: " "
    },
    THA399: {
      name: "Kanchanaburi",
      description: " "
    },
    THA400: {
      name: "Kamphaeng Phet",
      description: " "
    },
    THA401: {
      name: "Phichit",
      description: " "
    },
    THA402: {
      name: "Phetchabun",
      description: " "
    },
    THA403: {
      name: "Suphan Buri",
      description: " "
    },
    THA404: {
      name: "Tak",
      description: " "
    },
    THA405: {
      name: "Uthai Thani",
      description: " "
    },
    THA406: {
      name: "Ang Thong",
      description: " "
    },
    THA407: {
      name: "Chai Nat",
      description: " "
    },
    THA408: {
      name: "Lop Buri",
      description: " "
    },
    THA409: {
      name: "Nakhon Nayok",
      description: " "
    },
    THA410: {
      name: "Prachin Buri",
      description: " "
    },
    THA411: {
      name: "Nakhon Sawan",
      description: " "
    },
    THA412: {
      name: "Phra Nakhon Si Ayutthaya",
      description: " "
    },
    THA413: {
      name: "Pathum Thani",
      description: " "
    },
    THA414: {
      name: "Sing Buri",
      description: " "
    },
    THA415: {
      name: "Saraburi",
      description: " "
    },
    THA416: {
      name: "Bangkok Metropolis",
      description: " "
    },
    THA417: {
      name: "Nonthaburi",
      description: " "
    },
    THA418: {
      name: "Nakhon Pathom",
      description: " "
    },
    THA419: {
      name: "Phetchaburi",
      description: " "
    },
    THA420: {
      name: "Prachuap Khiri Khan",
      description: " "
    },
    THA421: {
      name: "Ratchaburi",
      description: " "
    },
    THA422: {
      name: "Samut Prakan",
      description: " "
    },
    THA423: {
      name: "Samut Sakhon",
      description: " "
    },
    THA424: {
      name: "Samut Songkhram",
      description: " "
    },
    THA425: {
      name: "Si Sa Ket",
      description: " "
    },
    THA426: {
      name: "Ubon Ratchathani",
      description: " "
    },
    THA427: {
      name: "Amnat Charoen",
      description: " "
    },
    THA428: {
      name: "Yasothon",
      description: " "
    },
    THA430: {
      name: "Chon Buri",
      description: " "
    },
    THA431: {
      name: "Chachoengsao",
      description: " "
    },
    THA432: {
      name: "Chanthaburi",
      description: " "
    },
    THA433: {
      name: "Sa Kaeo",
      description: " "
    },
    THA434: {
      name: "Rayong",
      description: " "
    },
    THA435: {
      name: "Trat",
      description: " "
    },
    THA436: {
      name: "Buri Ram",
      description: " "
    },
    THA437: {
      name: "Chaiyaphum",
      description: " "
    },
    THA438: {
      name: "Khon Kaen",
      description: " "
    },
    THA439: {
      name: "Kalasin",
      description: " "
    },
    THA440: {
      name: "Maha Sarakham",
      description: " "
    },
    THA441: {
      name: "Nakhon Ratchasima",
      description: " "
    },
    THA442: {
      name: "Roi Et",
      description: " "
    },
    THA443: {
      name: "Surin",
      description: " "
    },
    THA445: {
      name: "Loei",
      description: " "
    },
    THA446: {
      name: "Nong Khai",
      description: " "
    },
    THA447: {
      name: "Sakon Nakhon",
      description: " "
    },
    THA448: {
      name: "Udon Thani",
      description: " "
    },
    THA449: {
      name: "Nong Bua Lam Phu",
      description: " "
    },
    THA472: {
      name: "Nakhon Phanom",
      description: " "
    },
    THA473: {
      name: "Mukdahan",
      description: " "
    },
    THA493: {
      name: "Narathiwat",
      description: " "
    },
    THA494: {
      name: "Pattani",
      description: " "
    },
    THA5499: {
      name: "Bueng Kan",
      description: " "
    }
  },
  locations: {
    "0": {
      lat: "10.292469",
      lng: "99.0890763",
      name: "บริษัท กลุ่ม ปาล์ม ธรรมชาติ จํากัด",
      description: "12,000 ลูกบาศก์เมตรต่อวัน"
    },
    "1": {
      lat: "10.504995",
      lng: "98.977004",
      name: "บริษัท เจริญน้ำมันปาล์ม",
      description: "6,000 ลูกบาศก์เมตรต่อวัน"
    },
    "2": {
      lat: "8.9628375",
      lng: "99.2302999",
      description: "18,000 ลูกบาศก์เมตรต่อวัน",
      name: "บริษัท ทักษิณอุตสาหกรรมน้ำมันปาล์ม (1993) จำกัด"
    },
    "3": {
      lat: "9.604596",
      lng: "99.124706",
      name: "บริษัทท่าชนะน้ำมันปาล์มจำกัด",
      description: "15,750 ลูกบาศก์เมตรต่อวัน"
    },
    "4": {
      lat: "8.6333667",
      lng: "98.9570021",
      name: "บริษัท ไทยทาโลว์แอนด์ออยล์ จํากัด (สาขาบางสวรรค์)",
      description: "18,000 ลูกบาศก์เมตรต่อวัน"
    },
    "5": {
      lat: "9.5096502",
      lng: "99.133465",
      name: "บริษัท มิตรประสงค์กรีนเพาเวอร์ จำกัด ( บริษัท กลุ่มสมอทอง จำกัด)",
      description: "24,000 ลูกบาศก์เมตรต่อวัน"
    },
    "6": {
      lat: "10.2363944",
      lng: "99.1099574",
      name: "บริษัท เอเจ ปาล์มออยล์ จำกัด",
      description: "50,000 ลูกบาศก์เมตรต่อวัน"
    },
    "7": {
      lat: "9.604596",
      lng: "99.124706",
      name: "สหกรณ์นิคมท่าแซะ จํากัด",
      description: "8,000 ลูกบาศก์เมตรต่อวัน"
    }
  },
  labels: {
    "0": {
      name: "บริษัท ทักษิณอุตสาหกรรมน้ำมันปาล์ม (1993) จำกัด",
      x: 264.0519,
      y: 1496.0418,
      parent_type: "location",
      parent_id: "2"
    }
  },
  regions: {
    "0": {
      states: [
        "THA382",
        "THA375",
        "THA387",
        "THA376",
        "THA493",
        "THA494",
        "THA378",
        "THA383",
        "THA377",
        "THA388",
        "THA379",
        "THA386",
        "THA385",
        "THA380"
      ],
      name: "ภาคใต้",
      color: "#227cd4",
      hover_color: "#e8cc12",
      description: "กำลังการผลิด 151,750 ลูกบาศก์เมตรต่อวัน"
    },
    "1": {
      states: [],
      name: "ภาคกลาง",
      color: "#aa4601",
      hover_color: "#e8cc12"
    },
    "2": {
      states: [],
      name: "ภาคเหนือ"
    },
    "3": {
      states: [],
      name: "ภาคตะวันออกเฉียงเหนือ"
    },
    "4": {
      states: [],
      name: "ภาคตะวันออก"
    },
    "5": {
      states: [],
      name: "ภาคตะวันตก"
    }
  },
  data: {
    data: {}
  }
};