<?php session_start(); ?>
<?php 
include("css.php");?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>DEMO</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" src="map5/mapdata.js"></script>		
<script  type="text/javascript" src="map5/countrymap.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <?php include("header.php"); ?>
  <div class="content-wrapper">
  <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- /.content -->
    <section class="content">
      <div class="container-fluid">
       <?php include("Home_ds_page.php"); ?>
      </div>
    </div>
  </section>
  <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <?php include("side_menu.php"); ?>
    <!-- /.Main Sidebar Container -->
<?php include("footer.php"); ?> 
</div>

<!-- script -->
<?php include("script.php"); ?>

</body>
</html>