
  <!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="Lte_v3/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="Lte_v3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="Lte_v3/dist/js/adminlte.js"></script>
<script src="date/jquery-1.12.4.js"></script>
<script src="date/jquery-ui.js"></script>
<!-- OPTIONAL SCRIPTS -->
<!-- <script src="Lte_v3/plugins/chart.js/Chart.min.js"></script> -->
<script src="Lte_v3/dist/js/demo.js"></script>
<!-- <script src="Lte_v3/dist/js/pages/dashboard3.js"></script> -->
<!-- SweetAlert2 -->
<script src="Lte_v3/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Bootstrap 4 -->
<!-- <script src="Lte_v3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script> -->
<!-- <script src="date/js/bootstrap-datepicker.js"></script> -->
<script src="Lte_v3/plugins/datatables/jquery.dataTables.js"></script>
<script src="Lte_v3/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>


<script type="text/javascript">

$(function(){
	$("#dateInput").datepicker({
		dateFormat: 'yy-dd-mm'
  });
  $("#dateInput2").datepicker({
		dateFormat: 'yy-dd-mm'
  });
  $("#dateInput4").datepicker({
		dateFormat: 'yy-dd-mm'
  });
  $("#dateInput5").datepicker({
		dateFormat: 'yy-dd-mm'
  });


});

</script>
<!-- save student -->
<script type="text/javascript">
	$(function(){

		$('#register').click(function(e){
			var valid = this.form.checkValidity();
			if(valid){
      var student_code =$('#student_code').val();
      var class_code =$('#class_code').val();
      var program_code =$('#program_code').val();
			var Full_Name =$('#Full_Name').val();
      var Full_Name_Thai =$('#Full_Name_Thai').val();
      var Phone_Number  =$('#Phone_Number').val();
      var Id_card =$('#Id_card').val();
      var Job_position  =$('#Job_position').val();
      var Date_enrolled =$('#dateInput').val();
      var Address =$('#Address').val();
      var Full_Name_Father  =$('#Full_Name_Father').val();
      var Phone_Number_Father =$('#Phone_Number_Father').val();
      var Full_Name_Mother  =$('#Full_Name_Mother').val();
      var Phone_Number_Mother =$('#Phone_Number_Mother').val();
      var Hospital  =$('#Hospital').val();
      var img_student =$('#img_student').val();
      //alert(img_student);
      var img_father  =$('#img_father').val();
      var img_mother  =$('#img_mother').val();
      
				e.preventDefault();	

				$.ajax({
					type: 'POST',
					url: 'save_student.php',
          data: { student_code : student_code,
                  class_code:class_code,
                  program_code:program_code,
                  Full_Name : Full_Name,
                  Full_Name_Thai:Full_Name_Thai,
                  Phone_Number:Phone_Number,
                  Id_card:Id_card,
                  Job_position:Job_position,
                  Date_enrolled:Date_enrolled,
                  Address:Address,
                  Full_Name_Father:Full_Name_Father,
                  Phone_Number_Father:Phone_Number_Father,
                  Full_Name_Mother:Full_Name_Mother,
                  Phone_Number_Mother:Phone_Number_Mother,
                  Hospital:Hospital,
                  img_student:img_student,
                  img_father:img_father,
                  img_mother:img_mother},
					success: function(data){
					Swal.fire({
								'title': 'Successful',
								'text': data,
								'type': 'success'
								})
							
					},
					error: function(data){
						Swal.fire({
								'title': 'Errors',
								'text': 'There were errors while saving the data.',
								'type': 'error'
								}).then(function() {
    window.location = "reg_Student_page.php";
});
					}
				});

				
			}else{
				
			}

			



		});		

		
	});
	
</script>
<!-- /save student -->

<script type="text/javascript">

  $(function() {
    $('#example2').DataTable({
      'paging': true,
      'lengthChange': true,
      'searching': true,
      'ordering': true,
      'info': true,
      'autoWidth': true
	  
      
    })

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    $('.swalDefaultSuccess').click(function() {
      Swal.fire(
  'Success Record Data!',
  'You Clicked Button To Continue !',
  'success'
    )
    });


    $("#frm2").on("submit", function (event) {
      Swal.fire(
  'Success Record Data!',
  'You Clicked Button To Continue !',
  'error'
    )
    });

  });
  //Student_img
  jQuery(document).ready(function($) {
    $('#delete_image').hide();
    $('#edit_image').hide();
    $('[name="pickup1"]').hide();
    $('#delete_image2').hide();
    $('#edit_image2').hide();
    $('[name="pickup2"]').hide();
    $('#delete_image3').hide();
    $('#edit_image3').hide();
    $('[name="pickup3"]').hide();
    $('#delete_image4').hide();
    $('#edit_image4').hide();
    $('[name="Student_img"]').hide();
    $('#delete_image4_edit').show();
    $('#edit_image4_edit').show();
    $('[name="Student_img_edit"]').hide();
    $('#add_image4_edit').hide();

    $('#delete_image_1').show();
    $('#edit_image_1').show();
    $('[name="pickup1_1"]').hide();
    $('#add_image_1').hide();

    $('#delete_image_2').show();
    $('#edit_image_2').show();
    $('[name="pickup2_2"]').hide();
    $('#add_image_2').hide();

    $('#delete_image_3').show();
    $('#edit_image_3').show();
    $('[name="pickup3_3"]').hide();
    $('#add_image_3').hide();

    $('#delete_image_4').show();
    $('#edit_image_4').show();
    $('[name="pickup4_4"]').hide();
    $('#add_image_4').hide();


   function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#preview_image').show();
            $('#preview_image').attr('src', e.target.result);
            $('#add_image').hide();
            $('#edit_image').show();
            $('#delete_image').show();
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    function readURL2(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#preview_image2').show();
            $('#preview_image2').attr('src', e.target.result);
            $('#add_image2').hide();
            $('#edit_image2').show();
            $('#delete_image2').show();
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    function readURL3(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#preview_image3').show();
            $('#preview_image3').attr('src', e.target.result);
            $('#add_image3').hide();
            $('#edit_image3').show();
            $('#delete_image3').show();
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    function readURL4(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#preview_image4').show();
            $('#preview_image4').attr('src', e.target.result);
            $('#add_image4').hide();
            $('#edit_image4').show();
            $('#delete_image4').show();
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    function readURL5(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#preview_image4_edit').show();
            $('#preview_image4_edit').attr('src', e.target.result);
            $('#add_image4_edit').hide();
            $('#edit_image4_edit').show();
            $('#delete_image4_edit').show();
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    function readURL6(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#preview_image_1_1').show();
            $('#preview_image_1_1').attr('src', e.target.result);
            $('#add_image_1').hide();
            $('#edit_image_1').show();
            $('#delete_image_1').show();
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    function readURL7(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#preview_image_2_2').show();
            $('#preview_image_2_2').attr('src', e.target.result);
            $('#add_image_2').hide();
            $('#edit_image_2').show();
            $('#delete_image_2').show();
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    function readURL8(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#preview_image_3_3').show();
            $('#preview_image_3_3').attr('src', e.target.result);
            $('#add_image_3').hide();
            $('#edit_image_3').show();
            $('#delete_image_3').show();
        }
        reader.readAsDataURL(input.files[0]);
      }
    }


    $('[name="pickup1"]').change(function() {
      readURL(this);
    });
    $('[name="pickup2"]').change(function() {
      readURL2(this);
    });
    $('[name="pickup3"]').change(function() {
      readURL3(this);
    });
    $('[name="Student_img"]').change(function() {
      readURL4(this);
    });
    $('[name="Student_img_edit"]').change(function() {
      readURL5(this);
    });
    $('[name="pickup1_1"]').change(function() {
      readURL6(this);
    });
    $('[name="pickup2_2"]').change(function() {
      readURL7(this);
    });
    $('[name="pickup3_3"]').change(function() {
      readURL8(this);
    });

    $('#add_image,#edit_image').click(function(event) {
        $('[name="pickup1"]').trigger('click');
    });
    $('#add_image2,#edit_image2').click(function(event) {
        $('[name="pickup2"]').trigger('click');
    });
    $('#add_image3,#edit_image3').click(function(event) {
        $('[name="pickup3"]').trigger('click');
    });
    $('#add_image4,#edit_image4').click(function(event) {
        $('[name="Student_img"]').trigger('click');
    });
    $('#add_image4_edit,#edit_image4_edit').click(function(event) {
        $('[name="Student_img_edit"]').trigger('click');
    });
    $('#add_image_1,#edit_image_1').click(function(event) {
        $('[name="pickup1_1"]').trigger('click');
    });
    $('#add_image_2,#edit_image_2').click(function(event) {
        $('[name="pickup2_2"]').trigger('click');
    });
    $('#add_image_3,#edit_image_3').click(function(event) {
        $('[name="pickup3_3"]').trigger('click');
    });

    $('#delete_image').click(function(event) {
        $('[name="pickup1"]').val('');
        $('#preview_image').attr('src','');
        $('#preview_image').hide();
        $('#delete_image').hide();
        $('#edit_image').hide();
        $('#add_image').show();
    });
    $('#delete_image2').click(function(event) {
        $('[name="pickup2"]').val('');
        $('#preview_image2').attr('src','');
        $('#preview_image2').hide();
        $('#delete_image2').hide();
        $('#edit_image2').hide();
        $('#add_image2').show();
    });
    $('#delete_image3').click(function(event) {
        $('[name="pickup3"]').val('');
        $('#preview_image3').attr('src','');
        $('#preview_image3').hide();
        $('#delete_image3').hide();
        $('#edit_image3').hide();
        $('#add_image3').show();
    });
    $('#delete_image4').click(function(event) {
        $('[name="Student_img"]').val('');
        $('#preview_image4').attr('src','');
        $('#preview_image4').hide();
        $('#delete_image4').hide();
        $('#edit_image4').hide();
        $('#add_image4').show();
    });
    $('#delete_image4_edit').click(function(event) {
        $('[name="Student_img_edit"]').val('');
        $('#preview_image4_edit').attr('src','');
        $('#preview_image4_edit').hide();
        $('#delete_image4_edit').hide();
        $('#edit_image4_edit').hide();
        $('#add_image4_edit').show();
    });

    $('#delete_image_1').click(function(event) {
        $('[name="pickup1_1"]').val('');
        $('#preview_image_1_1').attr('src','');
        $('#preview_image_1_1').hide();
        $('#delete_image_1').hide();
        $('#edit_image_1').hide();
        $('#add_image_1').show();
    });

    $('#delete_image_2').click(function(event) {
        $('[name="pickup2_2"]').val('');
        $('#preview_image_2_2').attr('src','');
        $('#preview_image_2_2').hide();
        $('#delete_image_2').hide();
        $('#edit_image_2').hide();
        $('#add_image_2').show();
    });

    $('#delete_image_3').click(function(event) {
        $('[name="pickup3_3"]').val('');
        $('#preview_image_3_3').attr('src','');
        $('#preview_image_3_3').hide();
        $('#delete_image_3').hide();
        $('#edit_image_3').hide();
        $('#add_image_3').show();
    });

    

});


</script>


